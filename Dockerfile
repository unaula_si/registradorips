FROM openjdk:8-jdk-alpine
VOLUME [ "/home" ]
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=release", "/home/registradorips-0.0.1-SNAPSHOT.jar"]