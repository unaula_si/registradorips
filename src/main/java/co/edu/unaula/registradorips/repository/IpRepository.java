package co.edu.unaula.registradorips.repository;

import co.edu.unaula.registradorips.documents.IpDocument;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpRepository extends ReactiveMongoRepository<IpDocument, String> {
}
