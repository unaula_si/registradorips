package co.edu.unaula.registradorips.controller;

import co.edu.unaula.registradorips.controller.base.ControladorBase;
import co.edu.unaula.registradorips.converter.IpConverter;
import co.edu.unaula.registradorips.dto.IpDTO;
import co.edu.unaula.registradorips.service.IpService;
import co.edu.unaula.registradorips.utils.StringValidators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "api/")
public class IpController extends ControladorBase {
    private final IpService ipService;
    private final StringValidators stringValidators;

    @Autowired
    public IpController(IpService ipService, StringValidators stringValidators) {
        this.ipService = ipService;
        this.stringValidators = stringValidators;
    }

    @PostMapping(value = "ips")
    public Mono<ResponseEntity<IpDTO>> obtenerIps(@RequestParam(value = "ip") String ip) {
        return validadEntidad(stringValidators.validarNoTenerCaracteresEspeciales(ip)
                .flatMap(ipValida ->
                        ipService.guardarIp(IpConverter.convertirAModelCreacion(ip))
                                .map(IpConverter::convertirModelADto)));
    }
}