package co.edu.unaula.registradorips.service;

import co.edu.unaula.registradorips.converter.IpConverter;
import co.edu.unaula.registradorips.models.Ip;
import co.edu.unaula.registradorips.repository.IpRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class IpService {
    private final IpRepository ipRepository;

    @Autowired
    public IpService(IpRepository ipRepository) {
        this.ipRepository = ipRepository;
    }

    public Mono<Ip> guardarIp(Ip ip) {
        return ipRepository.save(IpConverter.convertirModelADocument(ip))
                .map(IpConverter::convertirDocumentAModel);
    }
}