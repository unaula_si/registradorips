package co.edu.unaula.registradorips.utils;

import co.edu.unaula.registradorips.exceptions.ValidacionCamposException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class StringValidators {
    @Value("${application.expresionIps}")
    private String expresionSoloIps;

    public Mono<List<String>> validarNoTenerCaracteresEspeciales(String... valores) {
        Pattern estructuraIp = Pattern.compile(expresionSoloIps);
        boolean tieneValoresConCaracteresEspeciales = !Arrays.stream(valores)
                .filter(valor ->
                        !estructuraIp.matcher(valor).matches())
                .collect(Collectors.toList())
                .isEmpty();
        if (tieneValoresConCaracteresEspeciales) {
            return Mono.error(ValidacionCamposException.Type.NO_ES_ESTRUCTURA_IP.build());
        }
        return Mono.just(Arrays.asList(valores));
    }
}
