package co.edu.unaula.registradorips.utils;

import java.util.UUID;

public class UniqueIDGenerator {
    public static String uuid() {
        return UUID.randomUUID().toString();
    }
}