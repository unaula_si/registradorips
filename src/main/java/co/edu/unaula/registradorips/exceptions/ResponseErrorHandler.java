package co.edu.unaula.registradorips.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ResponseErrorHandler {
    private Integer status;
    private String code;
    private String message;
}
