package co.edu.unaula.registradorips.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class CamposExceptionHandler {
    @ExceptionHandler(ValidacionCamposException.class)
    public ResponseEntity<ResponseErrorHandler> camposException(ValidacionCamposException exception) {
        return UltilExceptionControllerHandler.construirResponseException(400, exception.getType().getMessage(),"0");
    }
}
