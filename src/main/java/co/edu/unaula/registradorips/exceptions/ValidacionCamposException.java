package co.edu.unaula.registradorips.exceptions;

public class ValidacionCamposException extends RuntimeException {

    public enum Type {
        NO_ES_ESTRUCTURA_IP("Dato no es valida.");

        private final String message;

        public ValidacionCamposException build() {
            return new ValidacionCamposException(this);
        }

        Type(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

    }

    private final ValidacionCamposException.Type type;

    private ValidacionCamposException(ValidacionCamposException.Type type) {
        super(type.message);
        this.type = type;
    }

    public ValidacionCamposException.Type getType() {
        return type;
    }

}
