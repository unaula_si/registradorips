package co.edu.unaula.registradorips.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class IpDTO {
    private final String ip;
}