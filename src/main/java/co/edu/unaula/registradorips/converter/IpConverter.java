package co.edu.unaula.registradorips.converter;

import co.edu.unaula.registradorips.documents.IpDocument;
import co.edu.unaula.registradorips.dto.IpDTO;
import co.edu.unaula.registradorips.models.Ip;
import co.edu.unaula.registradorips.utils.UniqueIDGenerator;

public class IpConverter {
    public static Ip convertirDocumentAModel(IpDocument ipDocument){
        return Ip.builder()
                .ip(ipDocument.getIp())
                .build();
    }

    public static Ip convertirAModelCreacion(String ip){
        return Ip.builder()
                .ip(ip)
                .build();
    }

    public static IpDTO convertirModelADto(Ip ip){
        return IpDTO.builder()
                .ip(ip.getIp())
                .build();
    }

    public static IpDocument convertirModelADocument(Ip ip){
        IpDocument ipDocument = new IpDocument();
        ipDocument.setIp(ip.getIp());
        return ipDocument;
    }
}
